import React, { Component } from 'react';
import ReactTransitions from './tools/react-transitions/transitions';
import './tools/react-transitions/animations.css';
import './App.css';
import { Nav, NavItem, Navbar } from 'react-bootstrap';


import Background from './components/Background';
import Settings from './components/Settings';
import PoemPage from './components/PoemPage';
import About from './components/About';
// import logo from './images/logo2.svg';
import gitlab from './images/gitlab-logo.svg';

const Transitions = ReactTransitions.Transitions;

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			version: 'v1.11.2',
			currentPageId: 0,
			transitionStyle: "move-to-left-move-from-right",
			lines: 20,
			isPoemAllowed: false,
			pickedAlgorithm: 0
		};
		this.Comps = [];//Better leave this here
	};

	componentWillMount() {
		document.addEventListener("keydown", this.handleKeyPress);
	}

	componentWillUnmount() {
		document.removeEventListener("keydown", this.handleKeyPress);
	}

	handleKeyPress = (e) => {
		if (e.keyCode === 13 || e.charCode === 13 || e.keyCode === 87 || e.charCode === 87) {
			this.handleTransition(1)
		} else if ((e.keyCode === 81 || e.charCode === 81) && this.state.currentPageId !== 0) {
			this.handleTransition(0)
		} else if ((e.keyCode === 69 || e.charCode === 69) && this.state.currentPageId !== 2) {
			this.handleTransition(2)
		}
	}

	handleTransition = (selectedKey) => {
		if (selectedKey < this.state.currentPageId) {
			this.setState({
				currentPageId: selectedKey,
				transitionStyle: Transitions[Math.floor(Math.random() * Transitions.length)]
			});
		} else {
			this.setState({
				currentPageId: selectedKey,
				transitionStyle: Transitions[Math.floor(Math.random() * Transitions.length)]
			});
		}
	};

	onUpdate(linesNew, algorithmNew) {
		this.setState({
			lines: linesNew,
			pickedAlgorithm: algorithmNew,
			isPoemAllowed: true
		});
		this.handleTransition(1)
	}

	render() {
		/* Dunno why but works that way */
		let Comps = [
			<Settings onUpdate={this.onUpdate.bind(this)} lines={this.state.lines}
			          pickedAlgorithm={this.state.pickedAlgorithm}/>,
			<PoemPage lines={this.state.lines} pickedAlgorithm={this.state.pickedAlgorithm}/>,
			<About/>
		];
		const index = this.state.currentPageId % Comps.length;
		let comp = React.cloneElement(Comps[index], {key: index});

		return (
			<div className="App">
				<Background/>
				<div className="App-header">
					<h2>Welcome to poetry generator {this.state.version}</h2>
				</div>
				<div className="navigation">
					<Navbar collapseOnSelect>
						<Navbar.Header>
							<Navbar.Toggle />
						</Navbar.Header>
						<Navbar.Collapse>
							<Nav bsStyle="pills" activeKey={this.state.currentPageId}
							     onSelect={(selectedKey) => this.handleTransition(selectedKey)}>
								<NavItem eventKey={0} title="Settings">Options</NavItem>
								<NavItem eventKey={1} title="Poem" disabled={!this.state.isPoemAllowed}>Poem</NavItem>
								<NavItem eventKey={2} title="About">About</NavItem>
							</Nav>
						</Navbar.Collapse>
					</Navbar>
				</div>
				<div>
					<ReactTransitions
						transition={ this.state.transitionStyle }
						width={ '600px' }
						height={ '400px' }
						margin={ '0 auto' }
					>{comp}
					</ReactTransitions>
				</div>
				<div className="footer">
					<a href="https://gitlab.com/MikeDabrowski/reactive-poet" target="_blank" rel="noopener noreferrer">
						<img src={gitlab} className="GitLabLogo" alt="gitlablogo"/></a>
				</div>
			</div>
		)
	}
}
export default App;