/**
 * Created by Ahti on 17.03.2017.
 */
import React, {Component} from 'react';
import '../App.css';
import '../tools/react-transitions/animations.css';
import * as d3 from 'd3';

export default class Background extends Component {
	componentDidMount() {
		this.insertScript();
	}

	insertScript() {
		///////////////////////////////////////////////////////////////////////////
		////////////////////////////// Settings ///////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
		var width = "100%",
			height = "100%";
		var colours = ["#FFC800", "#C400BB", "#9423C9","#C400BB","#FFC800"];
		var offsets = [0,0.375,0.5,0.625,1];
		var speed = "60s";

		///////////////////////////////////////////////////////////////////////////
		/////////////////////////// Create the gradients //////////////////////////
		///////////////////////////////////////////////////////////////////////////
		var svg = d3.select("#app-bg")
			.append("svg")
			.attr("width", width)
			.attr("height", height)
			.append("g");

		var defs = svg.append("defs");
		var linearGradient = defs.append("linearGradient")
			.attr("id","animate-gradient")
			.attr("x1","0%")
			.attr("x2","200%")
			.attr("y1","0%")
			.attr("y2","-200%")
			.attr("spreadMethod", "reflect");

		linearGradient.selectAll(".stop")
			.data(colours)
			.enter().append("stop")
			.attr("offset", function(d,i) { return offsets[i]; })
			.attr("stop-color", function(d) { return d; });

		linearGradient.append("animate")
			.attr("attributeName","x1")
			.attr("values","0%;-200%")
			.attr("dur",speed)
			.attr("repeatCount","indefinite");

		linearGradient.append("animate")
			.attr("attributeName","x2")
			.attr("values","200%;0%")
			.attr("dur",speed)
			.attr("repeatCount","indefinite");

		linearGradient.append("animate")
			.attr("attributeName","y1")
			.attr("values","0%;200%")
			.attr("dur",speed)
			.attr("repeatCount","indefinite");

		linearGradient.append("animate")
			.attr("attributeName","y2")
			.attr("values","-200%;0%")
			.attr("dur",speed)
			.attr("repeatCount","indefinite");

		///////////////////////////////////////////////////////////////////////////
		////////////////////////// Create the rectangle ///////////////////////////
		///////////////////////////////////////////////////////////////////////////
		svg.append("rect")
			.attr("x", 0)
			.attr("y", 0)
			.attr("width", width)
			.attr("height", height)
			.style("fill", "url(#animate-gradient)");
	}

	render() {
		return (
			<div id="app-bg"></div>
		)
	}

}