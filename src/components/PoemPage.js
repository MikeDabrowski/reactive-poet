import React, {Component} from 'react';
import '../App.css';
import $ from 'jquery';
import '../tools/react-transitions/animations.css';
import Poem from './Poem';

class PoemPage extends Component {
	constructor(props) {
		super(props)
		this.state = {
			lines: this.props.lines,
			pickedAlgorithm: this.props.pickedAlgorithm
		}
	}

	componentDidMount() {
		let maxViewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
		let footerHeight = $('.footer').height();
		let parentElement = $('.tParent');
		let minHeightOfParentForFooterToBeAtTheBottom = maxViewPortHeight - parentElement.offset().top - footerHeight - 70;
		/* * * * * * * * * * * * * * * * * *
		 * 1. set footer 20px from the btm *
		 * 2. check if that hides content  *
		 *    y - expand                   *
		 * * * * * * * * * * * * * * * * * */
		parentElement.height(minHeightOfParentForFooterToBeAtTheBottom);
		let parentH = minHeightOfParentForFooterToBeAtTheBottom;
		let childH = $('.Poem').height();
		if (childH > parentH) {
			parentElement.height(childH + 20);
		}
	}

	render() {
		return (
			<div className="Poem">
				<Poem lines={this.props.lines} pickedAlgorithm={this.props.pickedAlgorithm} />
			</div>
		)
	}
}
export default PoemPage;