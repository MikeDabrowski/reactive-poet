/**
 * Created by Ahti on 11.05.2017.
 */
//Alright lets try strategy pattern
// Tu idą importy plikow slownikowych itp
import verse_template from '../dictionaries/tags/verse_template_v4.json'
import dic_nature from '../dictionaries/tags/dic_nature_v6.json'
import title_template from '../dictionaries/tags/title_template_v1.json'
//kolokacja
import pos_word from '../dictionaries/collocations/pos_word.json'
import pos_word_bi from '../dictionaries/collocations/pos_word_bi.json'
import pos_word_tri from '../dictionaries/collocations/pos_word_tri.json'


class Strategy {
	AlgorithmInterface() {
	}
}
class GenerateTitle {
	constructor(bundled) {
		this.wiersz = bundled.wierszyk;
		this.rafa = bundled.rafalove
	}

	getRandomInt = (min, max) => {
		return Math.floor(Math.random() * (max - min)) + min;
	};

	wygeneruj(algo) {
		// console.log(wybrany_template)
		let temp = this.rafa;
		let tytul = "";
		algo.template_control = 0;
		for (let i = 0; i < algo.wybrany_template1.length; i++) {
			for (let j = 0; j < temp.length; j++) {
				// console.log(temp[j].czm)
				// console.log(wybrany_template[i])
				if (temp[j].czm === algo.wybrany_template1[i]) {
					// console.log('znależiono')
					algo.template_control++;
					// console.log(temp[j].wyr)
					// console.log(template_control)
					// console.log(wybrany_template.length)
					tytul += (temp[j].wyr) + ' ';
					temp.splice(j, 1);
					break
				}
			}
		}
		return tytul
	}

	generate() {
		let algo = {
			wybrany_template1: '',
			template_control: 0,
		};
		let a = this.getRandomInt(0, title_template[1].length);
		algo.wybrany_template1 = title_template[1][a];

		let tytul = this.wygeneruj(algo);

		while (algo.template_control !== algo.wybrany_template1.length) {
			let a = this.getRandomInt(0, title_template[1].length);
			algo.wybrany_template1 = title_template[1][a];
			tytul = this.wygeneruj(algo)
		}

		return tytul
	}
}
class ConcreteStrategyA extends Strategy {
	constructor(lines) {
		//tu mozna parkowac zmienne 'globalne' dla calego algorytmu
		//linesy muszą być bo to oznacza ile linijek bedzie mial wiersz
		super();
		this.lastNoun = ''; //wiem, zmienna globalna, nieladnie. Ale inaczej by trzeba bylo 3 funkcje przerobic... Ej przerobilem Ci to i jest juz elegancko :D
		this.debug = false;
		this.lines = lines;
		this.rafa = []
	}

	/*Ta funkcja ma zwracac arraya z wierszem, kazdy element to osobna linijka
	 * Wszystkie funkcje jakie beda używane do generowania wiersza mogą byc obok (ES6)*/
	AlgorithmInterface() {
		console.log("using tags")
		this.rafa = [];
		let wiersz = [];
		for (let i = 0; i < this.lines; i++) {
			wiersz.push(this.generator(verse_template, dic_nature))
		}
		let bundled = {
			wierszyk: wiersz,
			rafalove: this.rafa

		};
		let tytul = new GenerateTitle(bundled).generate();
		wiersz.unshift(tytul);
		return bundled
	}

	losuj = (max) => {
		return Math.floor(Math.random() * (max + 1));
	};

	losujWyraz = (dic, type) => {
		let arrayOfWords = dic[type];
		//console.log('Tablice wyrazow', arrayOfWords);
		let atributesArray;
		if (arrayOfWords !== undefined) {
			atributesArray = Object.keys(arrayOfWords);
		} else {
			return type;// + ' ';
		}

		let sum = 0;
		for (let i = 0; i < atributesArray.length; i++) {
			sum += (atributesArray[i] * 1 * arrayOfWords[atributesArray[i]].length);
		}

		//console.log('Suma wag : ' + sum);
		let wylosowanaWagaWyrazow = this.losuj(sum);
		//console.log('Losowa waga wyrazów : ' + wylosowanaWagaWyrazow);

		let temp = 0;
		let wybranyWaga = 0;
		let tablicaWyrazow = 0;
		for (let i = 0; i < atributesArray.length; i++) {
			temp += (atributesArray[i] * 1 * arrayOfWords[atributesArray[i]].length);
			if (wylosowanaWagaWyrazow <= temp) {
				wybranyWaga = atributesArray[i];
				tablicaWyrazow = arrayOfWords[wybranyWaga];
				//console.log('Wybrana tablica wyrazow ! : ' + tablicaWyrazow);
				break;
			}
		}
		let indexWyrazu = this.losuj(tablicaWyrazow.length - 1);
		//console.log('Wybrany wyraz...' + tablicaWyrazow[indexWyrazu]);
		return tablicaWyrazow[indexWyrazu]; //tu dodawalo spacje, ale przerzucilem to do generatora (~121 linijka) ~pg.
	};

	generator = (template, dictionary) => {

		let wagiTemplate = Object.keys(template);
		let sum = 0;
		for (let i = 0; i < wagiTemplate.length; i++) {
			sum += (wagiTemplate[i] * 1 * template[wagiTemplate[i]].length);
		}

		if (this.debug) console.log('Suma wag : ' + sum);
		let wylosowanaWaga = this.losuj(sum);
		if (this.debug) console.log('Losowa waga : ' + wylosowanaWaga);
		let temp = 0;
		let wybranyTemplate = 0;
		for (let i = 0; i < wagiTemplate.length; i++) {
			temp += (wagiTemplate[i] * 1 * template[wagiTemplate[i]].length);
			if (wylosowanaWaga <= temp) {
				wybranyTemplate = wagiTemplate[i];
				if (this.debug) console.log('Wybrany rodzaj wagi template : ' + wybranyTemplate);
				break;
			}
		}
		let arrayOfChosenTemplates = template[wybranyTemplate];
		let chosenTemplate = arrayOfChosenTemplates[this.losuj(arrayOfChosenTemplates.length - 1)];

		let wers = '';
		if (this.debug) console.log('Wybrany template : ' + chosenTemplate);
		for (let i = 0; i < chosenTemplate.length; i++) {
			let rafaTuple = {
				wyr: '',
				czm: ''
			};
			//wers += this.losujWyraz(dictionary, chosenTemplate[i]);
			let wyraz = this.losujWyraz(dictionary, chosenTemplate[i]);

			//==================poprawianie gramatyki==================
			if (chosenTemplate[i] === "PRP" || chosenTemplate[i] === "NN") {
				this.lastNoun = wyraz;
//				console.log(wyraz);
			}
			else if (chosenTemplate[i] === "VB" || chosenTemplate[i] === "VBP") {
				if ("can" !== wyraz && "could" !== wyraz && "must" !== wyraz && "may" !== wyraz && "might" !== wyraz && "should" !== wyraz && "ought" !== wyraz && "ought to" !== wyraz) //modale sie nie zmieniaja
				{
					if ("be" === wyraz || "are" === wyraz || "am" === wyraz) //jezeli wylosowalem be, to sprawdzam ostatni wyraz w wersie i poprawiam forme be
					{
						if ('' === this.lastNoun) //dla bezpieczenstwa, prawdopodobnie nigdy nie uzyte
							wyraz = "is";
						else if ("s" === this.lastNoun[this.lastNoun.length - 1] || "you" === this.lastNoun || "we" === this.lastNoun || "they" === this.lastNoun) // be->are
							wyraz = "are";
						else if ("I" === this.lastNoun || 'i' === this.lastNoun) //be->am
							wyraz = "am";
						else //najczestszy przypadek wiec domyslnie
							wyraz = "is";
					}
					else if ("have" === wyraz) {
						if ("s" !== this.lastNoun[this.lastNoun.length - 1] || "he" === this.lastNoun || "she" === this.lastNoun || "it" === this.lastNoun) // have -> has jezeli l. poj. lub he/she/it
							wyraz = "has";
					}
					else if ("s" !== this.lastNoun[this.lastNoun.length - 1] && "I" !== this.lastNoun && "you" !== this.lastNoun) //jezeli rzeczownik byl w liczbie pojedynczej i nie bylo to "I" aniu "you" to +s
					{
						if ("x" === wyraz[wyraz.length - 1] || "s" === wyraz[wyraz.length - 1] || "h" === wyraz[wyraz.length - 1] || "go" === wyraz || "do" === wyraz) //jezeli jest go/do lub wyraz konczy sie na x/s/h to dodajemu es a nie s
							wyraz += "es";
						else
							wyraz += "s";
					}
				}
			} //koniec poprawiania
			rafaTuple.czm = chosenTemplate[i];
			rafaTuple.wyr = wyraz;
			this.rafa.push(rafaTuple);
			if (wyraz === ",") // usuwanie spacji przed przecinkami
				wers = wers.substr(0, wers.length - 1) + ", ";
			else
				wers += wyraz + " ";
		}
		if (this.debug) console.log('Wers: ');
		return wers;
	};
}
class ConcreteStrategyB extends Strategy {
	constructor(lines) {
		super();
		this.lines = lines
		this.rafa = []
	}

	AlgorithmInterface() {
		console.log("using collocations")
		this.rafa = [];
		let wiersz = this.generuj();
		let bundled = {
			wierszyk: wiersz,
			rafalove: this.rafa

		};

		// RAFA ODKOMENTUJ 2 NIZEJ , WYWAL 1 WYZEJ jak skonczysz swoja poprawke
		let tytul = new GenerateTitle(bundled).generate();
		wiersz.unshift(tytul);
		return bundled
	}

	shuffle = (array) => {
		let currentIndex = array.length, temporaryValue, randomIndex;

		// While there remain elements to this.shuffle...
		while (0 !== currentIndex) {

			// Pick a remaining element...
			randomIndex = Math.floor(Math.random() * currentIndex);
			currentIndex -= 1;

			// And swap it with the current element.
			temporaryValue = array[currentIndex];
			array[currentIndex] = array[randomIndex];
			array[randomIndex] = temporaryValue;
		}

		return array;
	};

	generuj = () => {
		let ab = "";
		let wiersz = [];

		let sstr = "";
		for (let j = 0; j < 1; j++) {
			ab = ab + "\n\n";
			for (let i = 0; i < this.lines * 4; i++) {
				let rafaTuple = {
					wyr: '',
					czm: ''
				};
				// let tes = lista[Math.floor(Math.random() * lista.length)]
				let tes = Math.round(Math.random()) + 1;
				if (i % 4 === 0) {
					if (tes === 0) {
						let abc = (pos_word[Math.floor(Math.random() * pos_word.length)]);
						ab = ab + " " + abc[0];
						rafaTuple.czm = abc[0][1];
						rafaTuple.wyr = abc[0];
						this.rafa.push(rafaTuple)
						sstr = abc[1].toString()
					}
					if (tes === 1) {
						let abc = pos_word_bi[Math.floor(Math.random() * pos_word_bi.length)];
						ab = ab + " " + abc[0][0] + " " + abc[1][0];
						rafaTuple.czm = abc[0][1];
						rafaTuple.wyr = abc[0][0];
						this.rafa.push(rafaTuple);
						rafaTuple = {
							wyr: '',
							czm: ''
						};
						rafaTuple.czm = abc[1][1];
						rafaTuple.wyr = abc[1][0];
						this.rafa.push(rafaTuple);
						rafaTuple = {
							wyr: '',
							czm: ''
						};
						sstr = abc[1][1].toString()
					}
					if (tes === 2) {
						let abc = pos_word_tri[Math.floor(Math.random() * pos_word_tri.length)];
						ab = ab + " " + abc[0][0] + " " + abc[1][0] + " " + abc[2][0];
						rafaTuple.czm = abc[0][1];
						rafaTuple.wyr = abc[0][0];
						this.rafa.push(rafaTuple)
						rafaTuple = {
							wyr: '',
							czm: ''
						};
						rafaTuple.czm = abc[1][1];
						rafaTuple.wyr = abc[1][0];
						this.rafa.push(rafaTuple)
						rafaTuple = {
							wyr: '',
							czm: ''
						};

						rafaTuple.czm = abc[2][1];
						rafaTuple.wyr = abc[2][0];


						this.rafa.push(rafaTuple)
						rafaTuple = {
							wyr: '',
							czm: ''
						};
						sstr = abc[2][1].toString()
					}
				} else if (i % 4 === 3) {
					wiersz.push(ab);
					ab = ""
				} else {
					this.shuffle(pos_word_bi);
					let tmp = '';
					for (let u = 0; u < pos_word_bi.length; u++) {
						if (pos_word_bi[u][0][1] === sstr) {
							tmp = pos_word_bi[u][1][1];
							break
						}
					}
					if (tes === 0) {
						this.shuffle(pos_word);
						let a1 = tmp;
						for (let o = 0; o < pos_word.length; o++) {
							if (pos_word[o][1] === tmp) {
								a1 = pos_word[o];
								break
							}
						}
						ab = ab + " " + a1[0].toString();
						rafaTuple.czm = a1[1];
						rafaTuple.wyr = a1[0];
						this.rafa.push(rafaTuple)
						rafaTuple = {
							wyr: '',
							czm: ''
						};
						sstr = a1[1].toString();
					}
					if (tes === 1) {
						this.shuffle(pos_word_bi);
						let abc = tmp;
						for (let p = 0; p < pos_word_bi.length; p++) {
							if (pos_word_bi[p][0][1] === tmp) {
								abc = pos_word_bi[p];
								break
							}
						}
						ab = ab + " " + abc[0][0] + " " + abc[1][0];
						rafaTuple.czm = abc[0][1];
						rafaTuple.wyr = abc[0][0];
						this.rafa.push(rafaTuple)
						rafaTuple = {
							wyr: '',
							czm: ''
						};
						rafaTuple.czm = abc[1][1];
						rafaTuple.wyr = abc[1][0];
						this.rafa.push(rafaTuple)
						rafaTuple = {
							wyr: '',
							czm: ''
						};
						sstr = abc[1][1].toString();
					}
					if (tes === 2) {
						this.shuffle(pos_word_tri);
						let abc = tmp;
						for (let k = 0; k < pos_word_tri.length; k++) {
							if (pos_word_tri[k][0][1] === tmp) {
								abc = pos_word_tri[k];
								break
							}
						}
						ab = ab + " " + abc[0][0] + " " + abc[1][0] + " " + abc[2][0];
						rafaTuple.czm = abc[0][1];
						rafaTuple.wyr = abc[0][0];
						this.rafa.push(rafaTuple)
						rafaTuple = {
							wyr: '',
							czm: ''
						};
						rafaTuple.czm = abc[1][1];
						rafaTuple.wyr = abc[1][0];
						this.rafa.push(rafaTuple)
						rafaTuple = {
							wyr: '',
							czm: ''
						};
						rafaTuple.czm = abc[2][1];
						rafaTuple.wyr = abc[2][0];
						this.rafa.push(rafaTuple)
						rafaTuple = {
							wyr: '',
							czm: ''
						};
						sstr = abc[2][1].toString();
					}
				}
			}
		}
		return wiersz
	}
}

export default class Algorithms {
	constructor(type, lines) {
		switch (type) {
			case 0:
				this.strategy = new ConcreteStrategyA(lines);
				break;
			case 1:
				this.strategy = new ConcreteStrategyB(lines);
				break;
			default:
				this.strategy = new ConcreteStrategyA(lines)
		}
	}

	ContextInterface() {
		return this.strategy.AlgorithmInterface()
	}
}
