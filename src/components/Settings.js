import React, { Component } from 'react';
import '../App.css';
import $ from 'jquery';
import '../tools/react-transitions/animations.css';
import {
	Button,
	Form,
	FormGroup,
	FormControl,
	ControlLabel,
} from 'react-bootstrap';
// import Cookies from 'universal-cookie';

export default class Settings extends Component {
	constructor(props) {
		super(props);
		this.state = {
			lines: this.props.lines,
			pickedAlgorithm: this.props.pickedAlgorithm
		};
		this.formData = {
			"lines": this.state.lines,
		}
	}


	componentDidMount() {
		let maxViewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
		let footerHeight = $('.footer').height();
		let parentElement = $('.tParent');
		let minHeightOfParentForFooterToBeAtTheBottom = maxViewPortHeight - parentElement.offset().top - footerHeight - 70;
		/* * * * * * * * * * * * * * * * * *
		 * 1. set footer 20px from the btm *
		 * 2. check if that hides content  *
		 *    y - expand                   *
		 * * * * * * * * * * * * * * * * * */
		parentElement.height(minHeightOfParentForFooterToBeAtTheBottom);
		let parentH = minHeightOfParentForFooterToBeAtTheBottom;
		let childH = $('.Settings').height();
		if (childH > parentH) {
			parentElement.height(childH + 20);
		}
	}

	updateAlgo = (event) => {
		this.setState({
			pickedAlgorithm: parseInt(event.target.value, 10)
		})
	}

	updateLines = (event) => {
		let lines = parseInt(event.target.value, 10)
		if (lines > 100) {
			lines = 100;
		}else if(isNaN(lines)||lines<5){
			lines = 5;
		}
		this.setState({
			lines: lines
		})
	}

	handleSubmit = (event) => {
		this.props.onUpdate(
			parseInt(this.state.lines, 10),
			parseInt(this.state.pickedAlgorithm, 10)
		);
		event.preventDefault();
		// this.handleCookie();
	}

	/*handleCookie = () => {
		const cookie = new Cookies();
		let c=cookie.get('achieved');
		if(typeof c !== 'undefined'){
			if(c>0){
				console.log(c);
				cookie.set('achieved', c++);
			}
		}else{
			cookie.set('achieved',1)
		}
	}*/

	render() {
		return (
			<div className="Page Settings">
				<Form className="form">
					<legend>Poem settings</legend>
					<FormGroup>
						<ControlLabel>Lines</ControlLabel>
						<FormControl value={this.state.lines}
						             onChange={this.updateLines}/>
					</FormGroup>
					<FormGroup controlId="formControlsSelect">
						<ControlLabel>Algorithm</ControlLabel>
						<FormControl componentClass="select"
						             placeholder="select"
						             value={this.state.pickedAlgorithm}
						             onChange={this.updateAlgo}>
							<option value="0">Tags</option>
							<option value="1">Collocations</option>
						</FormControl>
					</FormGroup>
					<Button type="submit" onClick={this.handleSubmit}>
						Submit
					</Button>
				</Form>
			</div>
		)
	}
}