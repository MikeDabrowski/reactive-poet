/**
 * Created by Ahti on 07.05.2017.
 */
import React, { Component } from 'react';
import '../App.css';
import Algorithms from '../components/Algorithms'

class Poem extends Component {
	constructor(props) {
		super(props);
		this.state = {
			lines: this.props.lines,
			pickedAlgorithm: this.props.pickedAlgorithm,
			poem: [],
			title: ''
		}
	}

	componentWillMount = () => {
		let newPoem = new Algorithms(this.state.pickedAlgorithm, this.state.lines).ContextInterface().wierszyk
		let newTitle = newPoem.shift()
		let capitalized = newTitle.charAt(0).toUpperCase() + newTitle.slice(1)
		this.setState({
			poem: newPoem,
			title: capitalized
		})
	}

	preparePoem = () => {
		return this.state.poem.map((line) =>
			<p key={this.state.poem.indexOf(line)}>{line}</p>
		);
	}

	render() {
		return (
			<div>
				<h2>{this.state.title}</h2>
				<div>
					{this.preparePoem()}
				</div>
			</div>
		)
	}
}
export default Poem;