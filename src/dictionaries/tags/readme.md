# Dictionary structure
Dictionaries are simple object defined with object literals. Structure is as follows:
```
{"N":{
	"20":"head",
	"40":"car
	},
"A":{
	"20":"fast"
	}
}
```
* N-noun
* V-verb
* A-adjective
* D-adverb
* O-other
