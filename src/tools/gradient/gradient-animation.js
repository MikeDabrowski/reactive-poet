/**
 * Created by Ahti on 13.03.2017.
 */
import $ from 'jquery';

var colors = [
	[255, 200, 0],
	[220, 103, 96],
	[196, 0, 187],
	[148, 35, 201]
];

var step = 0;
//color table indices for:
// current color left
// next color left
// current color right
// next color right
var colorIndices = [0, 1, 2, 3];

//transition speed
var gradientSpeed = 0.002 ;

function updateGradient() {

	if ($ === undefined) return;

	var c0_0 = colors[colorIndices[0]];
	var c0_1 = colors[colorIndices[1]];
	var c1_0 = colors[colorIndices[2]];
	var c1_1 = colors[colorIndices[3]];

	var istep = 1 - step;
	var r1 = Math.round(istep * c0_0[0] + step * c0_1[0]);
	var g1 = Math.round(istep * c0_0[1] + step * c0_1[1]);
	var b1 = Math.round(istep * c0_0[2] + step * c0_1[2]);
	var color1 = "rgb(" + r1 + "," + g1 + "," + b1 + ")";

	var r2 = Math.round(istep * c1_0[0] + step * c1_1[0]);
	var g2 = Math.round(istep * c1_0[1] + step * c1_1[1]);
	var b2 = Math.round(istep * c1_0[2] + step * c1_1[2]);
	var color2 = "rgb(" + r2 + "," + g2 + "," + b2 + ")";

	$('.app-bg').css({
		background: "-webkit-linear-gradient(-45deg, from(" + color1 + "), to(" + color2 + "))"
	}).css({
		background: "-moz-linear-gradient(left bottom, " + color1 + " 0%, " + color2 + " 100%)"
	});

	step += gradientSpeed;
	if (step >= 1) {
		step %= 1;
		colorIndices[0] = colorIndices[1];
		colorIndices[2] = colorIndices[3];

		//pick two new target color indices
		//do not pick the same as the current one
		colorIndices[1] = ( colorIndices[1] + Math.floor(1 + Math.random() * (colors.length - 1))) % colors.length;
		colorIndices[3] = ( colorIndices[3] + Math.floor(1 + Math.random() * (colors.length - 1))) % colors.length;

	}
}


var pos2=75;
var a=0,b=1,c=2;
function updateGradient3() {

	if ($ === undefined) return;

	//min offset from 0 or 100 - 5%
	var offset=1;
	pos2--;
	var temp;

	if(pos2<=offset){
		pos2=100-offset;
		temp=a;
		a=b;
		b=c;
		c=temp;
	}

	//3 color stops
	$('.app-bg').css({
	 background: "-webkit-linear-gradient(45deg, rgb(" + colors[a] + ") 0,rgb(" + colors[b] + ") "+pos2+"%,rgb(" + colors[c] + ") 100%)"
	 }).css({
	 background: "-moz-linear-gradient(45deg, rgb(" + colors[a] + ") 0,rgb(" + colors[b] + ") "+pos2+"%,rgb(" + colors[c] + ") 100%)"
	 });

}

/*var e = setInterval(updateGradient, 200);*/
