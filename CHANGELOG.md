v0.3 
 - footer now is dynamically positioned based on subpage content height
 - added gitlab project's link to footer
 
v0.4
 - added target="_blank"
 - completed gradient animation
 - fixed alt on img tags
 
v0.5
 - improved readability on Edge
 - IE will not be supported
 - remastered gradient animation
 
v0.6
 - Added algorithm in its early stage
 - Added poem component which has algorithm
 - Created quite nice data-flow; settings now have impact on algorithm
 - Removed syllables from settings
 
v0.7
 - Extended algorithm, tries to fix grammar mistakes
 - Enlarged dictionary

v0.75
 - Fixed large poems (again)
 - Added few minor things 
 
v0.8
 - strategy pattern for multi algo
 - stub to generate title function
 - improved About page
 - cleaned code a bit (removed useless imports)
 
v1.9.0
 - title generator
 - layout fixes
 - react upgrade
 - removed useless dependencies
 - updated dependencies
 - added depcheck to dev-dep.
 
v1.10.0
 - added collocations algorithm
 - max poem length - 100 lines
 - title generator for collocations not working yet
 
v1.10.1
 - removed title from collocations for good
 - no auto capitalization in collocations title ?
 
v1.11.0
 - title now works
 - keyboard (1,2,3) too
 - gif height fixed
 - reformatted code
 
v1.11.1
 - 123 > qwe
 - min 5 lines
 - wierszyk fixed

v1.11.2
 - removed logo
 - fixed dictionary (got rid of [])
 - removed gif